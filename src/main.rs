use clap::Clap;
use pixels::Error;

use std::thread;

mod agent;
mod colour;
mod draw;
mod genome;
mod model;
mod ui;

use genome::Genome;
use model::Model2D;

fn main() -> Result<(), Error> {
    #[cfg(not(target_arch = "wasm32"))]
    let cfg = ui::Config::parse(); // parameters from CLI (or defaults)

    let mut m = Model2D::new(
        cfg,
        genome::IdxGenome::new(), // type of genome
    );

    let (sender, reciever) = crossbeam_channel::unbounded();

    thread::spawn(move || loop {
        m.step();
        for i in 0..m.size() {
            sender.send(m.nmut(i)).unwrap();
        }
    });

    draw::run_window(cfg.width, cfg.height, reciever)?;
    Ok(())
}
