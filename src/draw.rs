use crate::colour::HSL;
use log::error;
use pixels::{Error, Pixels, SurfaceTexture};
use winit::dpi::LogicalSize;
use winit::event::{Event, VirtualKeyCode};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::WindowBuilder;
use winit_input_helper::WinitInputHelper;

pub fn run_window(
    width: u32,
    height: u32,
    r: crossbeam_channel::Receiver<u32>,
) -> Result<(), Error> {
    let event_loop = EventLoop::new();
    let mut input = WinitInputHelper::new();
    let window = {
        let size = LogicalSize::new(width as f64, height as f64);
        WindowBuilder::new()
            .with_title("HALO")
            .with_inner_size(size)
            .with_min_inner_size(size)
            .build(&event_loop)
            .unwrap()
    };
    let mut pixels = {
        let surface_texture = SurfaceTexture::new(width, height, &window);
        Pixels::new(width, height, surface_texture)?
    };

    let mut hue = true;

    event_loop.run(move |event, _, control_flow| {
        // Draw the current frame
        if let Event::RedrawRequested(_) = event {
            draw(&r, pixels.get_frame(), hue);
            if pixels
                .render()
                .map_err(|e| error!("pixels.render() failed: {}", e))
                .is_err()
            {
                *control_flow = ControlFlow::Exit;
                return;
            }
        }
        if input.update(&event) {
            if input.key_pressed(VirtualKeyCode::Escape) || input.quit() {
                *control_flow = ControlFlow::Exit;
                return;
            }
            if let Some(size) = input.window_resized() {
                pixels.resize(size.width, size.height);
            }

            if input.key_pressed(VirtualKeyCode::Space) {
                hue = !hue;
            }

            window.request_redraw();
        }
    });
}

fn draw(r: &crossbeam_channel::Receiver<u32>, frame: &mut [u8], hue: bool) {
    for pixel in frame.chunks_exact_mut(4) {
        let id = r.recv().unwrap();
        pixel.copy_from_slice(&id_to_rgba(id, hue));
    }
}

fn id_to_rgba(id: u32, hue: bool) -> [u8; 4] {
    if id == 0 {
        [255, 255, 255, 255]
    } else {
        let frac = id as f64 / 1200.0;
        if hue {
            HSL {
                h: frac,
                s: 0.8,
                l: 0.5,
            }
            .to_rgba()
        } else {
            HSL {
                h: 0.8,
                s: frac,
                l: frac,
            }
            .to_rgba()
        }
    }
}
