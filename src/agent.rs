use rand::prelude::*;

use crate::genome::Genome;

#[derive(Debug, Clone)]
pub struct Agent<G: Genome> {
    born: u32,
    parent_id: u32,
    id: u32,
    genome: G,
}

impl<G: Genome> Agent<G> {
    pub fn new(g: G) -> Self {
        Agent {
            born: 0,
            parent_id: 0,
            id: 1,
            genome: g,
        }
    }
    pub fn divide(&mut self, time: u32, id: u32) -> Self {
        let parent_id = self.id;
        self.born = time;
        self.parent_id = parent_id;
        self.id = id + 1;
        Agent {
            born: time,
            parent_id,
            id,
            genome: self.genome.divide(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Grid2D<G: Genome> {
    width: u32,
    height: u32,
    grid: Vec<Option<Agent<G>>>,
    population: u32,
    current_id: u32,
}

impl<G: Genome> Grid2D<G> {
    /// New grid `width` by `height` initialised to all `None`
    pub fn new(width: u32, height: u32, g: G) -> Self {
        let mut grid = Self {
            width,
            height,
            grid: vec![None; (width * height) as usize],
            population: 1,
            current_id: 1,
        };
        grid.set_point((width / 2, height / 2), Some(Agent::new(g)));
        grid
    }
    pub fn set_index(&mut self, idx: usize, value: Option<Agent<G>>) {
        if let Some(square) = self.grid.get_mut(idx) {
            *square = value
        }
    }
    /// Set the value of the grid at point `(x, y)` to `value`
    pub fn set_point(&mut self, p: (u32, u32), value: Option<Agent<G>>) {
        self.set_index(self.point_to_index(p), value)
    }
    fn point_to_index(&self, p: (u32, u32)) -> usize {
        point_to_index_2d(p, self.width)
    }
    fn index_to_point(&self, i: usize) -> (u32, u32) {
        index_to_point_2d(i, self.width)
    }
}

pub trait Grid {
    type Index;
    
    fn population(&self) -> u32;
    fn is_occupied(&self, index: Self::Index) -> bool;
    fn nmut_at(&self, index: Self::Index) -> u32;
    fn get_free_space(&mut self, index: Self::Index) -> Option<usize>;
    fn free(&mut self, index: Self::Index);
    fn try_divide_square(&mut self, index: Self::Index, current_ticks: u32);
}

impl<G: Genome> Grid for Grid2D<G> {
    type Index = usize;
    
    fn population(&self) -> u32 {
	self.population
    }
    fn is_occupied(&self, index: Self::Index) -> bool {
	self.grid.get(index).unwrap_or(&None).is_some()
    }
    fn nmut_at(&self, index: Self::Index) -> u32 {
	if let Some(Some(a)) = self.grid.get(index) {
            a.genome.nmut()
        } else {
            0
        }
    }
    fn get_free_space(&mut self, index: Self::Index) -> Option<usize> {
	nbh2d(self.index_to_point(index))
            .iter()
            .map(|p| self.point_to_index(*p))
            .filter(|&index| !self.is_occupied(index))
            .choose(&mut thread_rng())
    }
    fn free(&mut self, index: Self::Index) {
	if let Some(square) = self.grid.get_mut(index) {
            *square = None;
            self.population -= 1;
        }
    }
    fn try_divide_square(&mut self, index: Self::Index, current_tick: u32) {
	if let Some(space) = self.get_free_space(index) {
            let id = self.current_id;
	    
            let cell = self.grid.get_mut(index).unwrap().as_mut().unwrap();
            let daughter = cell.divide(current_tick, id);

            self.set_index(space, Some(daughter));
            self.population += 1;
            self.current_id += 2;
        }
    }
}

// moore neighborhood in 2D
fn nbh2d(p: (u32, u32)) -> [(u32, u32); 8] {
    let up_left = (p.0 - 1, p.1 + 1); // have it wrap even debug
    let up = (p.0, p.1 + 1);
    let up_right = (p.0 + 1, p.1 + 1);
    let right = (p.0 + 1, p.1);
    let down_right = (p.0 + 1, p.1 - 1);
    let down = (p.0, p.1 - 1);
    let down_left = (p.0 - 1, p.1 - 1);
    let left = (p.0 - 1, p.1);
    [
        up_left, up, up_right, right, down_right, down, down_left, left,
    ]
}

// von-neumann neighborhood in 2D
fn _vnm2d(p: (u32, u32)) -> [(u32, u32); 4] {
    let up = (p.0, p.1 + 1);
    let right = (p.0 + 1, p.1);
    let down = (p.0, p.1 - 1);
    let left = (p.0 - 1, p.1);
    [up, right, down, left]
}

fn index_to_point_2d(i: usize, width: u32) -> (u32, u32) {
    let y = i as u32 / width;
    let x = i as u32 % width;
    (x, y)
}

/// Internally, a zero-based coordinate `(x, y)` is represented as index `(height * x + y)`
fn point_to_index_2d(p: (u32, u32), width: u32) -> usize {
    (width * p.1 + p.0) as usize
}

// von-neumann neighborhood in 3D
fn _nbh3d(p: (u32, u32, u32)) -> [(u32, u32, u32); 6] { 
    [
        (p.0 + 1, p.1, p.2),
        (p.0 - 1, p.1, p.2),
        (p.0, p.1 + 1, p.2),
        (p.0, p.1 - 1, p.2),
        (p.0, p.1, p.2 + 1),
        (p.0, p.1, p.2 - 1),
    ]
}

fn _index_to_point_3d(i: usize, width: u32, height: u32) -> (u32, u32, u32) {
    let depth = i.div_euclid((width * height) as usize);
    let twod = i - depth * (width * height) as usize;
    let height = twod.div_euclid(width as usize);
    let width = i.rem_euclid(width as usize);

    (depth as u32, height as u32, width as u32)
}

fn _point_to_index_3d(p: (u32, u32, u32), width: u32, height: u32) -> usize {
    (height * width * p.0 + width * p.1 + p.2) as usize
}
