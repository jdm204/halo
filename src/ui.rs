use clap::Clap;

#[derive(Clap, Clone, Copy, Debug)]
#[clap(
    name = "HALO",
    about = "HALO, a derivative and WIP agent-based simulation framework!"
)]
pub struct Config {
    #[clap(long, default_value = "100")]
    pub width: u32,

    #[clap(long, default_value = "100")]
    pub height: u32,

    #[clap(long, default_value = "100")]
    pub depth: u32,

    #[clap(long = "p-death", default_value = "0.2")]
    pub pd: f64,
}
