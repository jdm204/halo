use rand::prelude::*;

use crate::agent::{Grid2D, Grid};
use crate::genome::Genome;
use crate::ui::Config;

#[derive(Debug, Clone)]
pub struct Model2D<G: Genome> {
    grid: Grid2D<G>,
    params: Config,
    iter_order: Vec<usize>,
    ticks: u32,
}

impl<G: Genome> Model2D<G> {
    pub fn new(params: Config, genome: G) -> Self {
        Model2D {
            grid: Grid2D::new(params.width, params.height, genome),
            iter_order: (0..(params.width * params.height) as usize).collect(),
            ticks: 0,
            params,
        }
    }
    pub fn nmut(&self, i: usize) -> u32 {
        self.grid.nmut_at(i)
    }
    pub fn size(&self) -> usize {
        (self.params.width * self.params.height) as usize
    }
    pub fn ticks(&self) -> u32 {
        self.ticks
    }
    pub fn population(&self) -> u32 {
        self.grid.population()
    }
    /// Step forward in the simulation
    pub fn step(&mut self) {
        self.iter_order.shuffle(&mut thread_rng());

        for &i in &self.iter_order {
            if self.grid.is_occupied(i) {
                let p_d = thread_rng().gen_range(0.0, 1.0);
                if p_d < self.params.pd - (self.nmut(i) as f64 / 6000.0) + 0.08 {
                    self.grid.free(i);
                    continue;
                }
                self.grid.try_divide_square(i, self.ticks);
            }
        }
        self.ticks += 1;

        if self.ticks % 50 == 0 {
            println!(
                "Population at {} ticks: {}",
                self.ticks(),
                self.population()
            );
        }
    }
}
