use rand::random;
use rand_distr::{Distribution, Poisson};

use std::fmt;

pub trait Genome: Clone + Sized + fmt::Display {
    fn new() -> Self;
    fn nmut(&self) -> u32;
    fn mutate(&mut self);
    fn divide(&mut self) -> Self;
}

#[derive(Debug, Clone)]
pub struct SimpleGenome {
    variants: u32,
}

impl Genome for SimpleGenome {
    fn new() -> Self {
        SimpleGenome { variants: 0 }
    }
    fn nmut(&self) -> u32 {
        self.variants
    }
    fn mutate(&mut self) {
        self.variants += 1;
    }
    fn divide(&mut self) -> Self {
        self.mutate();
        self.clone()
    }
}

impl fmt::Display for SimpleGenome {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} mutations", self.variants)
    }
}

#[derive(Debug, Clone)]
pub struct IdxGenome {
    pub variants: Vec<Variant>,
}

impl Genome for IdxGenome {
    fn new() -> Self {
        Self {
            variants: Vec::new(),
        }
    }
    fn nmut(&self) -> u32 {
        self.variants.len() as u32
    }
    fn mutate(&mut self) {
        // number of mutations drawn from poisson 3
        let n = Poisson::new(3.0).unwrap().sample(&mut rand::thread_rng());
        for _ in 0..n {
            self.variants.push(Variant::new(random(), random()));
        }
    }
    fn divide(&mut self) -> Self {
        let daughter_genome = self.variants.clone();

        let mut daughter = IdxGenome {
            variants: daughter_genome,
        };

        self.mutate();
        daughter.mutate();
        daughter
    }
}

impl fmt::Display for IdxGenome {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for v in &self.variants {
            write!(f, "{},", v)?
        }
        Ok(())
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Variant {
    pos: u32,
    alt: u8,
}

impl Variant {
    pub fn new(pos: u32, alt: u8) -> Self {
        Variant { pos, alt }
    }
}

impl fmt::Display for Variant {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match idx_chr(self.pos) {
            Some((chr, pos)) => write!(f, "{}:{}", chr, pos),
            None => write!(f, "?"),
        }
    }
}

#[derive(Debug)]
enum Chromosome {
    Chr1,
    Chr2,
    Chr3,
    Chr4,
    Chr5,
    Chr6,
    Chr7,
    Chr8,
    Chr9,
    Chr10,
    Chr11,
    Chr12,
    Chr13,
    Chr14,
    Chr15,
    Chr16,
    Chr17,
    Chr18,
    Chr19,
    Chr20,
    Chr21,
    Chr22,
    ChrX,
    ChrY,
    ChrMT,
}
use Chromosome::*;

impl fmt::Display for Chromosome {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Chr1 => "Chr1",
                Chr2 => "Chr2",
                Chr3 => "Chr3",
                Chr4 => "Chr4",
                Chr5 => "Chr5",
                Chr6 => "Chr6",
                Chr7 => "Chr7",
                Chr8 => "Chr8",
                Chr9 => "Chr9",
                Chr10 => "Chr10",
                Chr11 => "Chr11",
                Chr12 => "Chr12",
                Chr13 => "Chr13",
                Chr14 => "Chr14",
                Chr15 => "Chr15",
                Chr16 => "Chr16",
                Chr17 => "Chr17",
                Chr18 => "Chr18",
                Chr19 => "Chr19",
                Chr20 => "Chr20",
                Chr21 => "Chr21",
                Chr22 => "Chr22",
                ChrX => "ChrX",
                ChrY => "ChrY",
                ChrMT => "ChrMT",
            }
        )
    }
}

fn idx_chr(i: u32) -> Option<(Chromosome, u32)> {
    Some(match i {
        1..=248956422 => (Chr1, i),
        248956423..=491149951 => (Chr2, i - 1 - 248956423),
        491149952..=689445510 => (Chr3, i - 1 - 491149952),
        689445511..=879660065 => (Chr4, i - 1 - 689445511),
        879660066..=1061198324 => (Chr5, i - 1 - 879660066),
        1061198325..=1232004303 => (Chr6, i - 1 - 1061198325),
        1232004304..=1391350276 => (Chr7, i - 1 - 1232004304),
        1391350277..=1536488912 => (Chr8, i - 1 - 1391350277),
        1536488913..=1674883629 => (Chr9, i - 1 - 1536488913),
        1674883630..=1808681051 => (Chr10, i - 1 - 1674883630),
        1808681052..=1943767673 => (Chr11, i - 1 - 1808681052),
        1943767674..=2077042982 => (Chr12, i - 1 - 1943767674),
        2077042983..=2191407310 => (Chr13, i - 1 - 2077042983),
        2191407311..=2298451028 => (Chr14, i - 1 - 2191407311),
        2298451029..=2400442217 => (Chr15, i - 1 - 2298451029),
        2400442218..=2490780562 => (Chr16, i - 1 - 2400442218),
        2490780563..=2574038003 => (Chr17, i - 1 - 2490780563),
        2574038004..=2654411288 => (Chr18, i - 1 - 2574038004),
        2654411289..=2713028904 => (Chr19, i - 1 - 2654411289),
        2713028905..=2777473071 => (Chr20, i - 1 - 2713028905),
        2777473072..=2824183054 => (Chr21, i - 1 - 2777473072),
        2824183055..=2875001522 => (Chr22, i - 1 - 2824183055),
        2875001523..=3031042417 => (ChrX, i - 1 - 2875001523),
        3031042418..=3088269832 => (ChrY, i - 1 - 3031042418),
        3088269833..=3088286401 => (ChrMT, i - 1 - 3088269833),
        _ => return None,
    })
}
