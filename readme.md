# HALO: Hybrid Automata Library _Oxidised_

This is the beginning of an attempt to "re-write"
[HAL](https://github.com/MathOnco/HAL "Hybrid Automata Library
(Github)"), a java library for writing hybrid agent-based/PDE-based
models, in Rust.

This project is **in its early stages** and is mainly intended as a
**learning exercise** for its author, who has little experience in any
of the relevant domains. As such this code should not be used by
anyone.

Examples of serious modelling frameworks include
[HAL](https://github.com/MathOnco/HAL),
[PhysiCell](http://physicell.org/),
[CompuCell3D](https://compucell3d.org/),
[Mesa](https://github.com/projectmesa/mesa),
[Chaste](http://www.cs.ox.ac.uk/chaste/)
[Repast](https://repast.github.io/)
[Mason](https://cs.gmu.edu/~eclab/projects/mason/),
[NetLogo](https://ccl.northwestern.edu/netlogo/).

## Why 

There are many software packages for hybrid and CA modelling---my
reasons for starting HALO:

- better understand HAL and hybrid modelling
- explore Rust for agent-based modelling in biology
- improve my Rust programming

### Rust

I think Rust has amazing potential for high-performance scientific computing. 

- naturally competes with C/C++ in terms of performance
- modern tooling and infrastructure
  - `cargo install`, `cargo add` make using Rust code easy
  - Rust's compiler errors are _fantastic_
- Excellent support for parallelism / concurrency

## Current goals

- N-dimensional grids (unsure if this is partially blocked by Rust's 'const generics' feature)
- 2/3D real time visualisation
- exporting simulation results as a table / tree / how HAL does it for HAL -> EvoFreq

## Next steps

- Implement diffusion fields (ALO doesn't have the same ring)
- biopsies, plane samples
- site frequency spectra for more complex genome types

## Future goals for HALO

- to be _fast_ (I guess compare well against current frameworks in benchmarks)
- to be memory efficient
- to be friendly and flexible
  - a 'teaching mode' which walks the user through tumour growth / genetics / biopsy / modelling
  - webassembly interface
  - CLI interface for running 'experiments' without rebuilding
  - a library crate for more custom models
- reimplement a variety of published models

## Goals for if the future goals get done

In particular, features currently at the bottom of the list to implement:

- Off-lattice models
- Non-spatial models
- Anything that wouldn't be useful for cancer modelling specifically
- A website as sweet as <https://halloworld.org>
