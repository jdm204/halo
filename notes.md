On Ubuntu 16, setting up the pixel stuff failed (with Error:
AdapterNotFound) until I `apt install mesa-vulkan-drivers`.

Const generics is getting there though one can't yet do `[x; D * 2]`
where D is a const parameter to Struct for example. You'd want this to
make both the dimensions and size of each dimension generic, i.e `let
veclen = product(SHAPE)` for general tensors.
